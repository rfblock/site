## Personal Website
[![Netlify Status](https://api.netlify.com/api/v1/badges/02b5463f-64cd-4c6e-909b-5cf40c3d8a42/deploy-status)](https://app.netlify.com/sites/jacobhaap/deploys)

Greetings fellow humans! Welcome to the source of my personal site.
> Built with basic HTML, CSS, and JS. Media assets are pulled from [my assets repository](https://gitlab.com/j3x/assets).
